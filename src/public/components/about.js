/*
 * Copyright (c) 2016 Gabriel Augendre <gabriel@augendre.info>
 * Free software under MIT License. See LICENSE file.
 */

'use strict';
var React = require('react');

var About = React.createClass({
    render: function () {
        return (
            <div>
                <h2>About</h2>
                <p className="alert alert-info">
                    <strong>DAVIDS</strong> stands for DuplicAtes VIewer and Detector for Spotify.
                </p>
                <p>
                    This application allows you to find duplicates in your Spotify playlists.
                    For the moment, it does not offer to remove them for you.
                </p>
                <p>
                    The app requests access to :
                </p>
                <ul>
                    <li>
                        <strong>Your public information.</strong> I don't need this but it's here by default and I
                        can't remove it. I don't have access to your email address and I don't store any information
                        about you. I won't spam you.
                    </li>
                    <li>
                        <strong>Your private and collaborative playlists (read only).</strong> I need this to provide
                        the service on all of your playlists. Don't worry, I can't modify them and I don't store any
                        information about it.
                    </li>
                </ul>
                <h2>Source code</h2>
                <p>
                    This project is open source. You can view the source code on the <a
                    href="https://github.com/Crocmagnon/DAVIDS">GitHub repository</a>.
                </p>
                <h2>License</h2>
                <p>
                    This project is free software. It is released under the <a
                    href="https://opensource.org/licenses/MIT">MIT Expat License</a>. This means you can use it,
                    distribute it, modify it under the following conditions :
                </p>
                <ul>
                    <li>You have to include a copy of the license and copyright notices</li>
                    <li>You can't hold me liable</li>
                </ul>
                <p>
                    A copy of this license is included in the source code repository (see <a
                    href="https://github.com/Crocmagnon/DAVIDS/blob/master/LICENSE">LICENSE</a> file).
                </p>
                <h2>Contribute</h2>
                <p>
                    If you want to contribute to this project, feel free to <a
                    href="https://github.com/Crocmagnon/DAVIDS/issues">open an issue</a> or <a
                    href="https://github.com/Crocmagnon/DAVIDS/pulls">propose a pull-request</a>.
                </p>
                <h2>Other work</h2>
                <p>
                    You can find other projects I've worked on including school work on <a
                    href="https://github.com/Crocmagnon">my GitHub</a>.
                </p>
            </div>
        );
    }
});

module.exports = About;
