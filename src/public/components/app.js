/*
 * Copyright (c) 2016 Gabriel Augendre <gabriel@augendre.info>
 * Free software under MIT License. See LICENSE file.
 */

'use strict';
var React = require('react');
var $ = require('jquery');

var DuplicateFinderBox = require('./duplicate-finder-box');
var Authenticate = require('./authenticate');
var About = require('./about');

/**
 * Obtains parameters from the hash of the URL
 * @return Object
 */
function getHashParams() {
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
        q = window.location.hash.substring(1);
    while (e = r.exec(q)) {
        hashParams[e[1]] = decodeURIComponent(e[2]);
    }
    return hashParams;
}

var App = React.createClass({
    getInitialState: function () {
        var params = getHashParams();
        var access_token = params.access_token || null;
        var refresh_token = params.refresh_token || null;

        return {
            access_token: access_token,
            refresh_token: refresh_token,
            playlists: null,
            playlistsLoading: true,
            activeTab: {
                finder: true,
                about: false
            }
        };
    },
    getPlaylists: function () {
        this.setState({
            playlistsLoading: true
        });
        var self = this;
        $.ajax({
            url: '/get_playlists',
            data: {
                'access_token': this.state.access_token
            },
            success: function (data) {
                var pl = data.data;
                self.setState({
                    playlists: pl,
                    playlistsLoading: false
                });
            },
            error: function (xhr, response, err) {
                console.error(response, err);
            }
        });
    },
    isLoggedIn: function () {
        return !(this.state.access_token == null && this.state.refresh_token == null);
    },
    refreshAuth: function (access, refresh) {
        this.setState({
            access_token: access,
            refresh_token: refresh
        });

        if (!(access == null && refresh == null)) {
            this.getPlaylists();
        }
    },
    componentDidMount: function () {
        if (this.isLoggedIn()) {
            this.getPlaylists();
        }
    },
    showFinder: function (e) {
        e.preventDefault();
        this.setState({
            activeTab: {
                finder: true,
                about: false
            }
        });
    },
    showAbout: function (e) {
        e.preventDefault();
        this.setState({
            activeTab: {
                finder: false,
                about: true
            }
        });
    },
    render: function () {
        var auth = {
            access_token: this.state.access_token,
            refresh_token: this.state.refresh_token
        };
        var content;
        if (this.state.activeTab.finder) {
            content = (
                <div>
                    <h2>Login</h2>
                    <p>Please <a href="/login">login with Spotify</a> first :)</p>
                </div>
            );
            if (this.isLoggedIn()) {
                var playlistsLoading = this.state.playlistsLoading;
                content = (
                    <DuplicateFinderBox playlistsLoading={playlistsLoading} auth={auth}
                                        playlists={this.state.playlists}/>
                );
            }
        }
        else if (this.state.activeTab.about) {
            content = (<About />);
        }
        else {
            content = <p>Error...</p>
        }

        return (
            <div>
                <Authenticate activeTab={this.state.activeTab} showFinder={this.showFinder} showAbout={this.showAbout}
                              auth={auth} refreshAuth={this.refreshAuth}/>
                <div className="container-fluid">
                    <h1>DAVIDS <small className="hidden-sm hidden-xs">DuplicAtes VIewer and Detector for Spotify</small></h1>
                    {content}
                </div>
                <footer className="footer">
                    <div className="container-fluid">
                        <p className="text-muted">
                            Copyright © {(new Date()).getFullYear()} Gabriel Augendre &bull; <a
                            href="https://github.com/Crocmagnon/DAVIDS/tree/master/LICENSE">License (MIT)</a> &bull; <a
                            href="https://github.com/Crocmagnon/">Other work</a>
                        </p>
                    </div>
                </footer>
            </div>
        );
    }
});

module.exports = App;
