/*
 * Copyright (c) 2016 Gabriel Augendre <gabriel@augendre.info>
 * Free software under MIT License. See LICENSE file.
 */

'use strict';
var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var Duplicate = require('./duplicate');

var DuplicatesBox = React.createClass({
    render: function () {
        var ListGroup = ReactBootstrap.ListGroup;
        if (this.props.dups && this.props.dups.length > 0) {
            var duplicates = this.props.dups.map(function (duplicate) {
                return (
                    <Duplicate track={duplicate.track} key={duplicate.track.id}/>
                );
            });
            return (
                <ListGroup>
                    {duplicates}
                </ListGroup>
            );
        }
        else {
            return (
                <p>No duplicate found</p>
            );
        }
    }
});

module.exports = DuplicatesBox;
