/*
 * Copyright (c) 2016 Gabriel Augendre <gabriel@augendre.info>
 * Free software under MIT License. See LICENSE file.
 */

'use strict';
var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var $ = require('jquery');

var DuplicatesBox = require('./duplicates-box');
var PlaylistBox = require('./playlist-box');

var DuplicateFinderBox = React.createClass({
    getInitialState: function () {
        return {
            currentId: null,
            currentUId: null,
            dups: null,
            dupsLoading: false,
            clicked: false,
            title: null
        };
    },
    componentWillReceiveProps: function () {
        this.setState({
            clicked: false
        });
    },
    handlePlaylistClick: function (id, uid, title) {
        this.setState({
            currentId: id,
            currentUId: uid,
            dupsLoading: true,
            clicked: true,
            title: title
        });
        var self = this;

        $.ajax({
            url: "/pl/" + uid + "/" + id,
            data: {
                'access_token': self.props.auth.access_token
            },
            success: function (data) {
                var dups = data.data;
                self.setState({
                    dups: dups,
                    dupsLoading: false
                });
            },
            error: function (xhr, response, err) {
                console.error(response, err);
            }
        });
    },
    render: function () {
        var duplicates;
        var Col = ReactBootstrap.Col,
            Row = ReactBootstrap.Row;

        if (this.state.clicked) {
            duplicates = <p>Loading...</p>;
            if (!this.state.dupsLoading) {
                duplicates = <DuplicatesBox dups={this.state.dups}/>;
            }
            duplicates = (
                <Col md={9} sm={8}>
                    <h2>Duplicates in {this.state.title}</h2>
                    {duplicates}
                </Col>
            )
        }

        var playlistBox = <p>Loading...</p>;
        var dupsCount = this.state.dups && this.state.dups.length;
        if (!this.props.playlistsLoading) {
            playlistBox = <PlaylistBox handleClick={this.handlePlaylistClick} 
                                       dupsCount={dupsCount}
                                       dupsLoading={this.state.dupsLoading}
                                       playlists={this.props.playlists}/>;
        }

        return (
            <Row>
                <Col md={3} sm={4}>
                    <h2>Playlists</h2>
                    {playlistBox}
                </Col>
                {duplicates}
            </Row>
        );
    }
});

module.exports = DuplicateFinderBox;
