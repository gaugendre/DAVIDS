/*
 * Copyright (c) 2016 Gabriel Augendre <gabriel@augendre.info>
 * Free software under MIT License. See LICENSE file.
 */

'use strict';
var React = require('react');
var ReactBootstrap = require('react-bootstrap');

var PlaylistBox = React.createClass({
    getInitialState: function () {
        return {
            currentId: null
        };
    },
    clickOnItem: function (id, uid, title, event) {
        event.preventDefault();
        if (!this.props.dupsLoading) {
            this.setState({currentId: id});
            this.props.handleClick(id, uid, title);
        }
    },
    render: function () {
        var ListGroup = ReactBootstrap.ListGroup,
            ListGroupItem = ReactBootstrap.ListGroupItem;
        var currentId = this.state.currentId;

        var playlists = <p>No playlist found.</p>;
        if (this.props.playlists) {
            playlists = this.props.playlists.map(function (pl) {
                var id = pl.id;
                var active = currentId == id;
                var dupsCount = active ? !this.props.dupsLoading && this.props.dupsCount : "";
                return (
                    <ListGroupItem href="#"
                                   onClick={this.clickOnItem.bind(this, id, pl.owner.id, pl.name)}
                                   disabled={this.props.dupsLoading}
                                   active={active}
                                   key={id}>
                        {pl.name} <span className="badge">{dupsCount}</span>
                    </ListGroupItem>
                );
            }, this);
        }

        return (
            <ListGroup>
                {playlists}
            </ListGroup>
        );
    }
});

module.exports = PlaylistBox;
