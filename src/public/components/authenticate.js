/*
 * Copyright (c) 2016 Gabriel Augendre <gabriel@augendre.info>
 * Free software under MIT License. See LICENSE file.
 */

'use strict';
var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var $ = require('jquery');

var Authenticate = React.createClass({
    logout: function () {
        this.props.refreshAuth(null, null);
        window.location.href = "/";
    },
    refreshToken: function (event) {
        event.preventDefault();
        var self = this;
        $.ajax({
            url: '/refresh_token',
            data: {
                'refresh_token': self.props.auth.refresh_token
            },
            success: function (data) {
                self.props.refreshAuth(data.access_token, self.props.auth.refresh_token);
            },
            error: function (xhr, status, err) {
                console.error(status, err);
            }
        });
    },
    render: function () {
        var auth;
        var Nav = ReactBootstrap.Nav,
            NavItem = ReactBootstrap.NavItem,
            Navbar = ReactBootstrap.Navbar;

        if (this.props.auth.access_token == null) {
            auth = (
                <Nav pullRight>
                    <NavItem className="login" eventKey={1} href="/login">
                        <img className="spotify-logo" src="images/spotify-black.png" alt="Spotify Logo"/><strong>Login with Spotify</strong>
                    </NavItem>
                </Nav>
            );
        }
        else {
            auth = (
                <Nav pullRight>
                    <NavItem eventKey={2} onClick={this.refreshToken} href="#">Refresh my token</NavItem>
                    <NavItem eventKey={3} onClick={this.logout} href="#"><strong>Logout</strong></NavItem>
                </Nav>
            );
        }

        return (
            <Navbar staticTop={true} fluid={true}>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/" className="text-capitalize">DAVIDS</a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <NavItem eventKey={1} href="#" active={this.props.activeTab.finder}
                                 onClick={this.props.showFinder}>Finder</NavItem>
                        <NavItem eventKey={1} href="#" active={this.props.activeTab.about}
                                 onClick={this.props.showAbout}>About</NavItem>
                    </Nav>
                    {auth}
                </Navbar.Collapse>
            </Navbar>
        );
    }
});

module.exports = Authenticate;
