/*
 * Copyright (c) 2016 Gabriel Augendre <gabriel@augendre.info>
 * Free software under MIT License. See LICENSE file.
 */

'use strict';
var React = require('react');
var ReactBootstrap = require('react-bootstrap');

var Duplicate = React.createClass({
    render: function () {
        var ListGroupItem = ReactBootstrap.ListGroupItem;
        var authors = "";
        this.props.track.artists.forEach(function (item, index) {
            if (index != 0) {
                authors += ', ';
            }
            authors += item.name;
        });
        return (
            <ListGroupItem>{this.props.track.name} - {authors}</ListGroupItem>
        );
    }
});

module.exports = Duplicate;
