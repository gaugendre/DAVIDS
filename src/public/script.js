/*
 * Copyright (c) 2016 Gabriel Augendre <gabriel@augendre.info>
 * Free software under MIT License. See LICENSE file.
 */

'use strict';
window.$ = window.jQuery = require('jquery');
require('bootstrap');

var React = require('react');
var ReactDOM = require('react-dom');

var App = require('./components/app');

ReactDOM.render(
    <App />,
    document.getElementById('content')
);
